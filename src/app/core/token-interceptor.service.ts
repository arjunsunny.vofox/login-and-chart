import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor,HttpRequest,HttpHandler, HttpEvent } from '@angular/common/http'
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector){}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const re = /login/gi;
    // Exclude interceptor for login request:
    if (req.url.search(re) === -1 ) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      });
    }
    return next.handle(req);
  }
}
