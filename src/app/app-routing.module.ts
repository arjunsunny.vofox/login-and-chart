import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { AuthGuard } from './core/auth.guard';
import { LandingComponent } from './landing/landing.component';
import { GridComponent } from './grid-form/grid.component';
import { EmployeesComponent } from './employees/employees.component';
import { EditComponent } from './employees/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },

  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'landing',
    component: LandingComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'form',
    component: GridComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'employee',
    component: EmployeesComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'employee/add/:id',
    component: EditComponent ,
    canActivate: [AuthGuard]
  },

  {
    path: 'employee/edit/:id',
    component: EditComponent ,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
