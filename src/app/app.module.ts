import { AuthGuard } from './core/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './core/auth.service';
import { TokenInterceptorService } from './core/token-interceptor.service';
import { LandingComponent } from './landing/landing.component';
import { LoginService } from './login/login.service';
import { GridComponent } from './grid-form/grid.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeesService } from './services/employees.service';
import { EditComponent } from './employees/edit/edit.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingComponent,
    GridComponent,
    EmployeesComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [AuthService, AuthGuard, HttpClientModule,LoginService,EmployeesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
