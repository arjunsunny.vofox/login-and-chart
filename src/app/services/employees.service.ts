import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

@Injectable()

export class EmployeesService {

  employees: Employee[] = [

    {
      id: 1,
      name: 'Arjun',
      email: 'arjun@gmail.com',
      phone: 8281626378
    },
  
    {
      id: 2,
      name: 'Sunny',
      email: 'sunny@.gmail.com',
      phone: 9605499378
    }
  ];

  constructor() { }

  onGet(){
    return this.employees;
  }

  onGetEmployee(id: number){
    return this.employees.find(x=>x.id === id);
  }
  
  onAdd(employee: Employee){
    this.employees.push(employee)
  }

  onDelete(id: number){
    let employee = this.employees.find(x=>x.id === id);
    let index = this.employees.indexOf(employee,0)
    this.employees.splice(index, 1)
  }

  onUpdate(employee: Employee){
    let oldEmployee = this.employees.find(x=>x.id === employee.id);
    oldEmployee.name = employee.name;
    oldEmployee.email = employee.email;
    oldEmployee.phone = employee.phone;
  }
}
