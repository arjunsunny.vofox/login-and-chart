import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent {

  Consented : any;
  constructor(
    private loginService: LoginService) { }

    ngAfterViewInit() {
      this.loginService.getUpdates()
      .subscribe(
        res =>{
        //chart1
         let chart1 = am4core.create("pie-chart", am4charts.PieChart);
         let title1 = chart1.titles.create();
         title1.text = "Active Participants"
         chart1.paddingTop = 10;
         chart1.fontSize = 12;

         chart1.legend = new am4charts.Legend();
         chart1.legend.paddingTop = 20;
         chart1.data = res;

         let series1 = chart1.series.push(new am4charts.PieSeries());
         series1.dataFields.value = "ActiveParticipants";
         series1.dataFields.category = "NodeName";

         let pieTemplate = series1.slices.template;
         pieTemplate.tooltipText = "{category} : {value}"; 

          pieTemplate.events.on("hit", function(ev) {
          chart1.closeAllPopups();
          chart1.openPopup( 
           "Active Participants:"+ ev.target.dataItem.dataContext.ActiveParticipants + "<br>" +
           "Consented          :"+ ev.target.dataItem.dataContext.Consented + "<br>" +
           "Enrolled           :"+ ev.target.dataItem.dataContext.Enrolled + "<br>" +
           "In Progress        :"+ ev.target.dataItem.dataContext.InProgress + "<br>" +
           "Screen Failed      :"+ ev.target.dataItem.dataContext.ScreenFailed +"<br>" +
           "Terminated         :"+ ev.target.dataItem.dataContext.Terminated ,
          //  "Activation Date    :"+ ev.target.dataItem.dataContext.ActivationDate + "<br>" +
          //  "First Consent       :"+ ev.target.dataItem.dataContext.FirstConsent + "<br>" +
          //  "Last Consent       :"+ ev.target.dataItem.dataContext.LastConsent ,
           "<strong>"            + ev.target.dataItem.dataContext.NodeName+ "</strong>");
          });

         //chart2
          let chart2 = am4core.create("bar-Chart", am4charts.XYChart);
          chart2.colors.step = 2
          let title2 = chart2.titles.create();
          title2.text = "Enrolled"
          chart2.fontSize = 13;
          
          chart2.data = res;

          let categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "NodeName";
          
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.minGridDistance = 30;

          categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
            if (target.dataItem && target.dataItem.index & 2 == 2) {
              return dy + 25;
            }
            return dy;
          });

          let valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());

          // Create series
          let series2 = chart2.series.push(new am4charts.ColumnSeries());
          series2.dataFields.valueY = "Enrolled";
          series2.dataFields.categoryX = "NodeName";
          
          series2.name = "Enrolled";
          let columnTemplate = series2.columns.template;
          columnTemplate.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          columnTemplate.fillOpacity = .8;

          
          columnTemplate.strokeWidth = 2;
          columnTemplate.strokeOpacity = 1;

          columnTemplate.events.on("hit", function(ev) {
          chart2.closeAllPopups();
          chart2.openPopup(
           "Active Participants:"+ ev.target.dataItem.dataContext.ActiveParticipants + "<br>" +
           "Consented          :"+ ev.target.dataItem.dataContext.Consented + "<br>" +
           "Enrolled           :"+ ev.target.dataItem.dataContext.Enrolled + "<br>" +
           "In Progress        :"+ ev.target.dataItem.dataContext.InProgress + "<br>" +
           "Screen Failed      :"+ ev.target.dataItem.dataContext.ScreenFailed +"<br>" +
           "Terminated         :"+ ev.target.dataItem.dataContext.Terminated ,
           "<strong>"            + ev.target.dataItem.dataContext.NodeName+ "</strong>");
          });
        },
        err => console.log(err)
      )
    }

}