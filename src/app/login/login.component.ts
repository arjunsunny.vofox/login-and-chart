import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { Router } from '@angular/router'
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginUserData = {}
  resData: Object;
  emailAddress: any;
  userPassword : any;
  nodeUpdates : any[];
  tokenValue: any;
  loggedUserData: any;

  constructor(private _auth: AuthService,
              private _router: Router,
              private loginService : LoginService) { }

  loginUser () {
    
    let params = {
      EmailAddress : this.emailAddress,
      UserPassword : this.userPassword,
    }
    
    this._auth.loginUser(params)
    .subscribe(
      res => {
        localStorage.setItem('token', res)
        this._router.navigate(['/landing'])
      },
      err => console.log(err)
    ) 
  }
}
