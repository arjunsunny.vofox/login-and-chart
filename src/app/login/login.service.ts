import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class LoginService {

  private nodeUpdates = "http://192.168.7.106:8082//api/YDSRN/GetNodeUpdates";

  constructor(private http: HttpClient) { }

  getUpdates() {
    return this.http.get<any>(this.nodeUpdates)
  }

}